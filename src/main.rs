use anyhow::Result;
use directories::BaseDirs;
use std::env;
use std::path::{Path, PathBuf};
use ui::console::Console;

mod ts;
mod ui;
use ts::teamspeak::TeamSpeak;
use ui::home::HomeView;

const LOG_FILE_NAME: &str = "ncts.log";
const CONFIG_DIR_NAME: &str = "ncts";
const CONFIG_FILE_NAME: &str = "config.toml";
const KEY_FILE_NAME: &str = "keyfile";
const DEFAULT_SERVER_PORT: &str = "9987";

#[tokio::main]
async fn main() -> Result<()> {
    real_main().await
}

async fn real_main() -> Result<()> {
    tracing_subscriber::fmt::init();
    
    let mut server_address = String::new();
    let mut server_password = String::new();
    let mut server_port = String::from(DEFAULT_SERVER_PORT);
    let mut default_nickname = String::new();
    let mut default_channel = String::new();
    let mut default_channel_password = String::new();
    let mut config_dir = Path::new(BaseDirs::new().unwrap().config_dir()).join(CONFIG_DIR_NAME);
    let mut config_file = PathBuf::from(CONFIG_FILE_NAME);
    let mut key_file = PathBuf::from(KEY_FILE_NAME);
    let mut show_tui = true;

    //parse arguments
    let args: Vec<String> = env::args().collect();
    for (index, arg) in args.iter().enumerate() {
        match arg.as_str() {
            "-s" | "--serveraddress" => {
                server_address = if args.len() > index {
                    args[index + 1].clone()
                } else {
                    String::new()
                }
            }
            "-p" | "--server-password" => {
                server_password = if args.len() > index {
                    args[index + 1].clone()
                } else {
                    String::new()
                }
            }
            "-P" | "--server-port" => {
                server_port = if args.len() > index {
                    args[index + 1].clone()
                } else {
                    server_port
                }
            }
            "-n" | "--nickname" => {
                default_nickname = if args.len() > index {
                    args[index + 1].clone()
                } else {
                    String::new()
                }
            }
            "-c" | "--default-channel" => {
                default_channel = if args.len() > index {
                    args[index + 1].clone()
                } else {
                    String::new()
                }
            }
            "-C" | "--default-channel-password" => {
                default_channel_password = if args.len() > index {
                    args[index + 1].clone()
                } else {
                    String::new()
                }
            }
            "--config-dir" => {
                config_dir = if args.len() > index {
                    PathBuf::from(args[index + 1].clone())
                } else {
                    config_dir
                }
            }
            "--config" => {
                config_file = if args.len() > index {
                    PathBuf::from(args[index + 1].clone())
                } else {
                    PathBuf::from(CONFIG_FILE_NAME)
                }
            }
            "-k" | "--key-file" => {
                key_file = if args.len() > index {
                    PathBuf::from(args[index + 1].clone())
                } else {
                    PathBuf::from(KEY_FILE_NAME)
                }
            }
            "-t" | "--terminal-only" | "--no-tui" => show_tui = false,
            _ => {}
        }
    }

    if show_tui {
        //startup tui
        let ui = HomeView::new(None, config_dir, key_file);
        ui.show();
    } else {
        //connect to teamspeak server
        let teamspeak = TeamSpeak::new(
            server_address,
            server_password,
            server_port,
            default_nickname,
            default_channel,
            default_channel_password,
            config_dir,
            key_file,
        );
        let mut console = Console::new(teamspeak);
        console.start().await?;
    }

    Ok(())
}
